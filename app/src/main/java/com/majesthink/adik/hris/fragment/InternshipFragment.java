package com.majesthink.adik.hris.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.majesthink.adik.hris.R;

public class InternshipFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstance){

        View view = inflater.inflate(R.layout.fragment_internship, viewGroup, false);
        return view;
    }
}
