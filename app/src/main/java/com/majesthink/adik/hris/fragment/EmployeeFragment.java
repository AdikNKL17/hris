package com.majesthink.adik.hris.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.majesthink.adik.hris.R;
import com.majesthink.adik.hris.VerticalItemDecoration;
import com.majesthink.adik.hris.adapter.EmployeeAdapter;
import com.majesthink.adik.hris.model.Employee;

import java.util.ArrayList;
import java.util.List;

public class EmployeeFragment extends Fragment {

    RecyclerView employeeRecycler;

    List<Employee> employeeList;
    EmployeeAdapter employeeAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup viewGroup, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_employee, viewGroup, false);

        employeeList = new ArrayList<>();
        employeeRecycler = view.findViewById(R.id.employee_recycler);
        employeeAdapter = new EmployeeAdapter(view.getContext(), employeeList);
        employeeRecycler.setLayoutManager(new LinearLayoutManager(view.getContext(), LinearLayoutManager.VERTICAL, false));
        employeeRecycler.setItemAnimator(new DefaultItemAnimator());
        employeeRecycler.setAdapter(employeeAdapter);

        int margin = getResources().getDimensionPixelSize(R.dimen.recycler_margin);
        employeeRecycler.addItemDecoration(new VerticalItemDecoration(margin));

        dataEmployee();

        return view;
    }

    private void dataEmployee() {
        Employee employee = new Employee(0, 1810012591, 1810012591, "Audrey", "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQVogazXdK842P9xCxvcEBF4R8XNF6BMFjgRoDJZJx1otGFMb0c", "Accounting", "0862324245", true);
        employeeList.add(employee);
        employee = new Employee(1, 1810012591, 1810012591, "Franscisco", "https://pbs.twimg.com/profile_images/624538070059253760/05sGQxGK.jpg", "Accounting", "0862324245", true);
        employeeList.add(employee);
        employee = new Employee(2, 1810012591, 1810012591, "Abraham", "https://www.aasi.or.id/assets/pages/media/profile/profile_user.jpg", "Accounting", "0862324245", true);
        employeeList.add(employee);
        employee = new Employee(3, 1810012591, 1810012591, "Moses Abdee", "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRzgNXwA85Vjuw2yKY1LwgR_babCXdSPY-UMlyyBeSpVMzgVf6_", "Accounting", "0862324245", true);
        employeeList.add(employee);
        employee = new Employee(4, 1810012591, 1810012591, "Kylee Quinn", "http://gpluseurope.com/wp-content/uploads/Website2016-Profile-Photos-Amanda-Orza-300x261.jpg", "Accounting", "0862324245", true);
        employeeList.add(employee);
    }

}
