package com.majesthink.adik.hris.model;

public class Kpi {

    private int id;
    private String IMG_URL, employee_name, employee_jabatan;

    public Kpi(){

    }

    public Kpi(int id, String IMG_URL, String employee_name, String employee_jabatan){
        this.id = id;
        this.IMG_URL = IMG_URL;
        this.employee_name = employee_name;
        this.employee_jabatan = employee_jabatan;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIMG_URL() {
        return IMG_URL;
    }

    public void setIMG_URL(String IMG_URL) {
        this.IMG_URL = IMG_URL;
    }

    public String getEmployee_name() {
        return employee_name;
    }

    public void setEmployee_name(String employee_name) {
        this.employee_name = employee_name;
    }

    public String getEmployee_jabatan() {
        return employee_jabatan;
    }

    public void setEmployee_jabatan(String employee_jabatan) {
        this.employee_jabatan = employee_jabatan;
    }
}
