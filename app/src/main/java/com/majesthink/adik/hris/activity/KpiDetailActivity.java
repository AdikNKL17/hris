package com.majesthink.adik.hris.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.majesthink.adik.hris.R;
import com.majesthink.adik.hris.VerticalItemDecoration;
import com.majesthink.adik.hris.adapter.KpiDetailAdapter;
import com.majesthink.adik.hris.model.KpiDetail;
import com.makeramen.roundedimageview.RoundedImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class KpiDetailActivity extends AppCompatActivity {

    List<KpiDetail> kpiDetailList;
    KpiDetailAdapter kpiDetailAdapter;

    Toolbar toolbar;

    RoundedImageView imageProfile;
    TextView employeeeName, employeeJabatan;

    RecyclerView kpiDetailRecycler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kpi_detail);

        toolbar = findViewById(R.id.toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        kpiDetailList = new ArrayList<>();
        kpiDetailAdapter = new KpiDetailAdapter(KpiDetailActivity.this, kpiDetailList);

        imageProfile = findViewById(R.id.image_profile);
        employeeeName = findViewById(R.id.text_employee);
        employeeJabatan = findViewById(R.id.text_jabatan);

        kpiDetailRecycler = findViewById(R.id.kpi_detail_recycler);

        kpiDetailRecycler.setLayoutManager(new LinearLayoutManager(KpiDetailActivity.this, LinearLayoutManager.VERTICAL, false));
        kpiDetailRecycler.setItemAnimator(new DefaultItemAnimator());
        kpiDetailRecycler.setAdapter(kpiDetailAdapter);
        int margin = getResources().getDimensionPixelSize(R.dimen.recycler_margin);
        kpiDetailRecycler.addItemDecoration(new VerticalItemDecoration(margin));

        loadDataIntent();
        dataListKPI();
    }

    private void dataListKPI() {
        KpiDetail kpiDetail = new KpiDetail(0, "https://img.icons8.com/color/1600/java-coffee-cup-logo.png", "JAVA");
        kpiDetailList.add(kpiDetail);
        kpiDetail = new KpiDetail(1, "https://img.icons8.com/color/1600/js.png", "JavaScript");
        kpiDetailList.add(kpiDetail);
        kpiDetail = new KpiDetail(2, "https://img.icons8.com/color/1600/php.png", "PHP");
        kpiDetailList.add(kpiDetail);
        kpiDetail = new KpiDetail(3, "https://img.icons8.com/color/1600/css3.png", "CSS");
        kpiDetailList.add(kpiDetail);
        kpiDetail = new KpiDetail(4, "https://img.icons8.com/color/1600/html-5.png", "HTML");
        kpiDetailList.add(kpiDetail);
        kpiDetail = new KpiDetail(3, "https://img.icons8.com/color/1600/css3.png", "CSS");
        kpiDetailList.add(kpiDetail);
        kpiDetail = new KpiDetail(4, "https://img.icons8.com/color/1600/html-5.png", "HTML");
        kpiDetailList.add(kpiDetail);
    }

    private void loadDataIntent() {

        String IMG_URL, employee_name, employee_jabatan;

        IMG_URL = getIntent().getStringExtra("IMAGE_PROFILE");
        employee_name = getIntent().getStringExtra("EMPLOYEE_NAME");
        employee_jabatan = getIntent().getStringExtra("EMPLOYEE_JABATAN");

        Picasso.get()
                .load(IMG_URL)
                .fit()
                .centerCrop()
                .into(imageProfile);

        employeeeName.setText(employee_name);
        employeeJabatan.setText(employee_jabatan);

    }
}
