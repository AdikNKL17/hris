package com.majesthink.adik.hris.activity;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.majesthink.adik.hris.R;
import com.majesthink.adik.hris.adapter.TabAdapter;
import com.majesthink.adik.hris.fragment.EmployeeFragment;
import com.majesthink.adik.hris.fragment.InternshipFragment;

public class EmployeeActivity extends AppCompatActivity {

    Toolbar toolbar;
    TabLayout tabLayout;
    TabAdapter tabAdapter;
    ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employee);

        toolbar = findViewById(R.id.toolbar);
        tabLayout = findViewById(R.id.tabs);
        viewPager = findViewById(R.id.employee_view_pager);

        toolbar.inflateMenu(R.menu.toolbar_menu);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        tabAdapter = new TabAdapter(getSupportFragmentManager());
        tabAdapter.addFragment(new EmployeeFragment(), "Employee");
        tabAdapter.addFragment(new InternshipFragment(), "Magang");

        viewPager.setAdapter(tabAdapter);
        tabLayout.setupWithViewPager(viewPager);
    }
}
