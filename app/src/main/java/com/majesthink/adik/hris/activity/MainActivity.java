package com.majesthink.adik.hris.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageButton;

import com.majesthink.adik.hris.R;

public class MainActivity extends AppCompatActivity {

    Toolbar mtoolbar;
    ImageButton button_home, button_absensi, button_employee, button_kpi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mtoolbar = findViewById(R.id.toolbar);

        mtoolbar.inflateMenu(R.menu.toolbar_menu);

        button_home = findViewById(R.id.button_home);
        button_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, HomeActivity.class);
                startActivity(intent);
            }
        });

        button_absensi = findViewById(R.id.button_absensi);
        button_absensi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, AbsensiActivity.class);
                startActivity(intent);
            }
        });

        button_employee = findViewById(R.id.button_employee);
        button_employee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, EmployeeActivity.class);
                startActivity(intent);
            }
        });

        button_kpi = findViewById(R.id.button_kpi);
        button_kpi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, KpiActivity.class);
                startActivity(intent);
            }
        });


    }

}
