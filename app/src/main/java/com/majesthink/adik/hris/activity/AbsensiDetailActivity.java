package com.majesthink.adik.hris.activity;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.majesthink.adik.hris.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import ru.slybeaver.slycalendarview.SlyCalendarDialog;

public class AbsensiDetailActivity extends AppCompatActivity implements SlyCalendarDialog.Callback  {

    Button period;
    TextView mulai, akhir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_absensi_detail);

        period = findViewById(R.id.period);

        mulai = findViewById(R.id.mulai);
        akhir = findViewById(R.id.akhir);

        period.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new SlyCalendarDialog()
                        .setSingle(false)
                        .setCallback(AbsensiDetailActivity.this)
                        .setHeaderColor(Color.parseColor("#9d0927"))
                        .setSelectedColor(Color.parseColor("#9d0927"))
                        .show(getSupportFragmentManager(), "PERIOD");
            }
        });



    }

    @Override
    public void onCancelled() {

    }

    @Override
    public void onDataSelected(Calendar tanggalMulai, Calendar tanggalAkhir, int jam, int menit) {

        if (tanggalMulai != null) {
            if (tanggalAkhir == null) {
                tanggalMulai.set(Calendar.HOUR_OF_DAY, jam);
                tanggalMulai.set(Calendar.MINUTE, menit);
                Toast.makeText(
                        this,
                        new SimpleDateFormat(getString(R.string.timeFormat), Locale.getDefault()).format(tanggalMulai.getTime()),
                        Toast.LENGTH_LONG

                ).show();
            } else {
                Toast.makeText(
                        this,
                        getString(
                                R.string.period,
                                new SimpleDateFormat(getString(R.string.dateFormat), Locale.getDefault()).format(tanggalMulai.getTime()),
                                new SimpleDateFormat(getString(R.string.dateFormat), Locale.getDefault()).format(tanggalAkhir.getTime())
                        ),
                        Toast.LENGTH_LONG

                ).show();

                mulai.setText(new SimpleDateFormat(getString(R.string.dateFormat), Locale.getDefault()).format(tanggalMulai.getTime()));
                akhir.setText(new SimpleDateFormat(getString(R.string.dateFormat), Locale.getDefault()).format(tanggalAkhir.getTime()));
            }
        }

    }
}
