package com.majesthink.adik.hris.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.majesthink.adik.hris.R;
import com.majesthink.adik.hris.activity.AbsensiDetailActivity;
import com.majesthink.adik.hris.model.Absensi;
import com.makeramen.roundedimageview.RoundedImageView;
import com.squareup.picasso.Picasso;

import java.util.List;

public class AbsensiAdapter extends RecyclerView.Adapter<AbsensiAdapter.AbsensiHolder> {

    Context context;
    List<Absensi> absensiList;

    @NonNull
    @Override
    public AbsensiHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_absensi, viewGroup, false);
        return new AbsensiHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AbsensiHolder absensiHolder, int i) {

        final Absensi absensi = absensiList.get(i);

        Picasso.get()
                .load(absensiList.get(i).getIMG_URL())
                .fit()
                .centerCrop()
                .into(absensiHolder.employeeImage);

        absensiHolder.employeeName.setText(absensi.getEmployee_name());
        absensiHolder.employeeJabatan.setText(absensi.getEmployee_jabatan());

        absensiHolder.absensiHolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "Clicked", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(v.getContext(), AbsensiDetailActivity.class);
                v.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return absensiList.size();
    }

    public class AbsensiHolder extends RecyclerView.ViewHolder {

        CardView absensiHolder;
        RoundedImageView employeeImage;
        TextView employeeName, employeeJabatan;

        public AbsensiHolder(@NonNull View itemView) {
            super(itemView);
            absensiHolder = itemView.findViewById(R.id.absensi_holder);
            employeeImage = itemView.findViewById(R.id.image_profile);
            employeeName = itemView.findViewById(R.id.text_employee);
            employeeJabatan = itemView.findViewById(R.id.text_jabatan);

        }
    }

    public AbsensiAdapter(Context context, List<Absensi> absensiList){

        this.context = context;
        this.absensiList = absensiList;

    }
}
