package com.majesthink.adik.hris.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.majesthink.adik.hris.R;
import com.majesthink.adik.hris.activity.EmployeeDetail;
import com.majesthink.adik.hris.model.Employee;
import com.makeramen.roundedimageview.RoundedImageView;
import com.squareup.picasso.Picasso;

import java.util.List;

public class EmployeeAdapter extends RecyclerView.Adapter<EmployeeAdapter.EmployeeHolder> {

    Context context;
    List<Employee> employeeList;

    @NonNull
    @Override
    public EmployeeHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_employee, viewGroup, false);

        return new EmployeeHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull EmployeeHolder employeeHolder, int i) {
        final Employee employee = employeeList.get(i);

        Picasso.get()
                .load(employee.getEMPLOYEE_IMAGE())
                .fit()
                .centerCrop()
                .into(employeeHolder.employeeImage);

        employeeHolder.employeeName.setText(employee.getEmployeeName());
        employeeHolder.employeeJabatan.setText(employee.getEmployeeJabatan());
        employeeHolder.employeeNip.setText(Integer.toString(employee.getEmployeeNip()));

        employeeHolder.employeeHolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "Clicked", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(v.getContext(), EmployeeDetail.class);
                v.getContext().startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return employeeList.size();
    }

    public class EmployeeHolder extends RecyclerView.ViewHolder {

        CardView employeeHolder;
        RoundedImageView employeeImage;
        TextView employeeName, employeeJabatan, employeeNip;

        public EmployeeHolder(@NonNull View itemView) {
            super(itemView);

            employeeHolder = itemView.findViewById(R.id.employee_holder);
            employeeImage = itemView.findViewById(R.id.image_profile);
            employeeName = itemView.findViewById(R.id.employee_name);
            employeeJabatan = itemView.findViewById(R.id.employee_jabatan);
            employeeNip = itemView.findViewById(R.id.employee_nip);
        }
    }

    public EmployeeAdapter(Context context, List<Employee> employeeList){
        this.context = context;
        this.employeeList = employeeList;
    }
}
