package com.majesthink.adik.hris.model;

public class KpiDetail {

    int id;
    String KPI_ICON, kpi_name;

    public KpiDetail(){

    }

    public KpiDetail(int id, String KPI_ICON, String kpi_name) {
        this.id = id;
        this.KPI_ICON = KPI_ICON;
        this.kpi_name = kpi_name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getKPI_ICON() {
        return KPI_ICON;
    }

    public void setKPI_ICON(String KPI_ICON) {
        this.KPI_ICON = KPI_ICON;
    }

    public String getKpi_name() {
        return kpi_name;
    }

    public void setKpi_name(String kpi_name) {
        this.kpi_name = kpi_name;
    }
}
