package com.majesthink.adik.hris.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.majesthink.adik.hris.R;
import com.majesthink.adik.hris.model.KpiDetail;
import com.squareup.picasso.Picasso;

import java.util.List;

public class KpiDetailAdapter extends RecyclerView.Adapter<KpiDetailAdapter.KpiDetailHolder> {

    Context context;
    List<KpiDetail> kpiDetailList;

    @NonNull
    @Override
    public KpiDetailHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_kpi_detail, viewGroup, false);
        return new KpiDetailHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull KpiDetailHolder kpiDetailHolder, int i) {

        KpiDetail kpiDetail = kpiDetailList.get(i);

        Picasso.get()
                .load(kpiDetail.getKPI_ICON())
                .fit()
                .centerCrop()
                .into(kpiDetailHolder.kpiDetailIcon);

        kpiDetailHolder.kpiDetailName.setText(kpiDetail.getKpi_name());

    }

    @Override
    public int getItemCount() {
        return kpiDetailList.size();
    }

    public class KpiDetailHolder extends RecyclerView.ViewHolder {
        ImageView kpiDetailIcon;
        TextView kpiDetailName;
        public KpiDetailHolder(@NonNull View itemView) {
            super(itemView);

            kpiDetailIcon = itemView.findViewById(R.id.icon_image);
            kpiDetailName = itemView.findViewById(R.id.kpi_name);

        }
    }

    public KpiDetailAdapter(Context context, List<KpiDetail> kpiDetailList){
        this.context = context;
        this.kpiDetailList = kpiDetailList;
    }
}
