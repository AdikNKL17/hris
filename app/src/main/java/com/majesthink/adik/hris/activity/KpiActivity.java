package com.majesthink.adik.hris.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.majesthink.adik.hris.R;
import com.majesthink.adik.hris.VerticalItemDecoration;
import com.majesthink.adik.hris.adapter.KpiAdapter;
import com.majesthink.adik.hris.model.Kpi;

import java.util.ArrayList;
import java.util.List;

public class KpiActivity extends AppCompatActivity {

    Toolbar toolbar;
    List<Kpi> kpiList;
    KpiAdapter kpiAdapter;
    RecyclerView kpiRecycler;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kpi);

        toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        kpiList = new ArrayList<>();
        kpiAdapter = new KpiAdapter(KpiActivity.this, kpiList);

        kpiRecycler = findViewById(R.id.kpi_recycler);
        kpiRecycler.setItemAnimator(new DefaultItemAnimator());
        kpiRecycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        kpiRecycler.setAdapter(kpiAdapter);
        int margin = getResources().getDimensionPixelSize(R.dimen.recycler_margin);

        kpiRecycler.addItemDecoration(new VerticalItemDecoration(margin));

        dataKpi();
    }

    private void dataKpi() {
        Kpi kpi = new Kpi(1023211, "https://cdn2.stylecraze.com/wp-content/uploads/2013/07/105-Top-Knot-Tutorial-With-Detailed-Steps-And-Pictures.jpg", "Palapa Beta Argadia", "Lead Developer");
        kpiList.add(kpi);
        kpi = new Kpi(1023212, "https://therighthairstyles.com/wp-content/uploads/2016/02/3-messy-top-knot-hairstyle.jpg", "Irfan Al Azhari", "Project Manager");
        kpiList.add(kpi);
        kpi = new Kpi(1023213, "https://2.bp.blogspot.com/-3winRHNClQ0/VubbN1-wD4I/AAAAAAAAB-U/Hz2K7qs7EQQHnkM96QJhb6puo6p11NpXw/s1600/natasha%2Brizki.jpg", "Biyono", "Fullstack Developer");
        kpiList.add(kpi);
        kpi = new Kpi(1023214, "https://img2.thejournal.ie/inline/2470754/original/?width=428&version=2470754", "Risman Wahyudi", "Backend Developer");
        kpiList.add(kpi);
        kpi = new Kpi(1023215, "https://www.linkedinsights.com/wp-content/uploads/2012/12/Screen_shot_2012-04-29_at_9.34.13_PM1.png", "Irfangi", "Backend Developer");
        kpiList.add(kpi);
    }
}
