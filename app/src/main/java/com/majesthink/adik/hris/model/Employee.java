package com.majesthink.adik.hris.model;

public class Employee {
    private int id, employeeNip, employeeKtp;
    private String employeeName, EMPLOYEE_IMAGE, employeeJabatan, employeePhone;
    private boolean employeeStatus;

    public Employee(){

    }

    public Employee(int id, int employeeNip, int employeeKtp,  String employeeName, String EMPLOYEE_IMAGE, String employeeJabatan, String employeePhone, boolean employeeStatus){

        this.id = id;
        this.employeeNip = employeeNip;
        this.employeeKtp = employeeKtp;
        this.employeeName = employeeName;
        this.EMPLOYEE_IMAGE = EMPLOYEE_IMAGE;
        this.employeeJabatan = employeeJabatan;
        this.employeePhone = employeePhone;
        this.employeeStatus = employeeStatus;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getEmployeeNip() {
        return employeeNip;
    }

    public void setEmployeeNip(int employeeNip) {
        this.employeeNip = employeeNip;
    }

    public int getEmployeeKtp() {
        return employeeKtp;
    }

    public void setEmployeeKtp(int employeeKtp) {
        this.employeeKtp = employeeKtp;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getEMPLOYEE_IMAGE() {
        return EMPLOYEE_IMAGE;
    }

    public void setEMPLOYEE_IMAGE(String EMPLOYEE_IMAGE) {
        this.EMPLOYEE_IMAGE = EMPLOYEE_IMAGE;
    }

    public String getEmployeeJabatan() {
        return employeeJabatan;
    }

    public void setEmployeeJabatan(String employeeJabatan) {
        this.employeeJabatan = employeeJabatan;
    }

    public String getEmployeePhone() {
        return employeePhone;
    }

    public void setEmployeePhone(String employeePhone) {
        this.employeePhone = employeePhone;
    }

    public boolean getEmployeeStatus() {
        return employeeStatus;
    }

    public void setEmployeeStatus(boolean employeeStatus) {
        this.employeeStatus = employeeStatus;
    }
}
