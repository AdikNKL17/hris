package com.majesthink.adik.hris.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.majesthink.adik.hris.R;
import com.majesthink.adik.hris.activity.KpiDetailActivity;
import com.majesthink.adik.hris.model.Kpi;
import com.makeramen.roundedimageview.RoundedImageView;
import com.squareup.picasso.Picasso;

import java.util.List;

public class KpiAdapter extends RecyclerView.Adapter<KpiAdapter.KpiHolder> {

    Context context;
    List<Kpi> kpiList;

    @NonNull
    @Override
    public KpiHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_kpi, viewGroup, false);

        return new KpiHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull KpiHolder kpiHolder, int i) {
        final Kpi kpi = kpiList.get(i);

        Picasso.get()
                .load(kpi.getIMG_URL())
                .fit()
                .centerCrop()
                .into(kpiHolder.image_profile);

        kpiHolder.text_employee.setText(kpi.getEmployee_name());
        kpiHolder.text_jabatan.setText(kpi.getEmployee_jabatan());

        kpiHolder.kpiHolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), KpiDetailActivity.class);
                intent.putExtra("IMAGE_PROFILE", kpi.getIMG_URL());
                intent.putExtra("EMPLOYEE_NAME", kpi.getEmployee_name());
                intent.putExtra("EMPLOYEE_JABATAN", kpi.getEmployee_jabatan());

                v.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return kpiList.size();
    }

    public class KpiHolder extends RecyclerView.ViewHolder {

        CardView kpiHolder;
        RoundedImageView image_profile;
        TextView text_employee, text_jabatan;

        public KpiHolder(@NonNull View itemView) {
            super(itemView);

            kpiHolder = itemView.findViewById(R.id.kpi_holder);
            image_profile = itemView.findViewById(R.id.image_profile);
            text_employee = itemView.findViewById(R.id.text_employee);
            text_jabatan = itemView.findViewById(R.id.text_jabatan);
        }
    }

    public KpiAdapter(Context context, List<Kpi> kpiList){
        this.context = context;
        this.kpiList = kpiList;
    }
}
