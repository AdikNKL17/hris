package com.majesthink.adik.hris.activity;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.majesthink.adik.hris.R;
import com.majesthink.adik.hris.VerticalItemDecoration;
import com.majesthink.adik.hris.adapter.AbsensiAdapter;
import com.majesthink.adik.hris.model.Absensi;

import java.util.ArrayList;
import java.util.List;

public class AbsensiActivity extends AppCompatActivity {

    Toolbar toolbar;

    List<Absensi> absensiList;
    AbsensiAdapter absensiAdapter;

    RecyclerView absensiRecycler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_absensi);

        toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        absensiList = new ArrayList<>();
       /* absensiList = new ArrayList<>();
        absensiRecycler = findViewById(R.id.absensi_recycler);
        absensiAdapter = new AbsensiAdapter(this, absensiList);
        absensiRecycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        absensiRecycler.setAdapter(absensiAdapter);*/
        loadAbsensi();
        dataAbsensi();
    }

    private void loadAbsensi() {
        absensiRecycler = findViewById(R.id.absensi_recycler);
        absensiAdapter = new AbsensiAdapter(AbsensiActivity.this, absensiList);
        absensiRecycler.setLayoutManager(new LinearLayoutManager(AbsensiActivity.this, LinearLayoutManager.VERTICAL, false));
        absensiRecycler.setItemAnimator(new DefaultItemAnimator());
        absensiRecycler.setAdapter(absensiAdapter);

        int margin = getResources().getDimensionPixelSize(R.dimen.recycler_margin);

        absensiRecycler.addItemDecoration(new VerticalItemDecoration(margin));
    }

    private void dataAbsensi() {
        Absensi absensi = new Absensi(1023211, "https://cdn2.stylecraze.com/wp-content/uploads/2013/07/105-Top-Knot-Tutorial-With-Detailed-Steps-And-Pictures.jpg", "Palapa Beta Argadia", "Lead Developer");
        absensiList.add(absensi);
        absensi = new Absensi(1023212, "https://therighthairstyles.com/wp-content/uploads/2016/02/3-messy-top-knot-hairstyle.jpg", "Irfan Al Azhari", "Project Manager");
        absensiList.add(absensi);
        absensi = new Absensi(1023213, "https://2.bp.blogspot.com/-3winRHNClQ0/VubbN1-wD4I/AAAAAAAAB-U/Hz2K7qs7EQQHnkM96QJhb6puo6p11NpXw/s1600/natasha%2Brizki.jpg", "Biyono", "Full Stack Developer");
        absensiList.add(absensi);
        absensi = new Absensi(1023214, "https://img2.thejournal.ie/inline/2470754/original/?width=428&version=2470754", "Risman Wahyudi", "Backend Developer");
        absensiList.add(absensi);
        absensi = new Absensi(1023215, "https://www.linkedinsights.com/wp-content/uploads/2012/12/Screen_shot_2012-04-29_at_9.34.13_PM1.png", "Irfangi", "Backend Developer");
        absensiList.add(absensi);
        absensi = new Absensi(1023216, "https://griffonagedotcom.files.wordpress.com/2016/07/profile-modern-2e.jpg", "Dovi Alexander", "Android Developer");
        absensiList.add(absensi);
        absensi = new Absensi(1023217, "https://cdn.business2community.com/wp-content/uploads/2014/04/profile-picture-300x300.jpg", "Anggita", "Backend Developer");
        absensiList.add(absensi);
        absensi = new Absensi(1023218, "https://sguru.org/wp-content/uploads/2017/06/29cac887f27845d1ab97d3bd4523b976.jpg", "Roy Alexander", "Marketing");
        absensiList.add(absensi);
        absensi = new Absensi(1023219, "https://static-cdn.jtvnw.net/jtv_user_pictures/89ab44ce-f2fa-4d5a-bd16-ca717d9fc125-profile_image-300x300.png", "Devina", "Accounting");
        absensiList.add(absensi);
        absensi = new Absensi(1023220, "https://cdn.fashionmagazine.com/wp-content/uploads/2014/06/how-to-take-the-perfect-profile-picture-margaret-zhang-twitter-480x0-c-default.jpg", "Alexa", "IT Support");
        absensiList.add(absensi);
        absensi = new Absensi(1023221, "https://static1.squarespace.com/static/59c21f76a9db092ebfab5ae0/t/5aa1ecc60d92974ee6f5afdf/1520579075783/Michelle+Chong%27s+Photo+3+%28Please+credit+Michelle+as+Left+Profile+Artiste%29+%282%29.JPG", "Galvano", "Recruiter");
        absensiList.add(absensi);
    }
}
